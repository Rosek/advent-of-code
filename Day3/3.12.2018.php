<?php

$file = new SplFileObject("in.txt");

while (!$file->eof()) {
    $in[] = $file->fgets();
}

unset($file);

$elements = $names = $claims = [];
$count = 0;

foreach ($in as $value) {
    if (!$value)
        continue;

    $tmp = explode(' ', $value);
    $names[] = $name = $tmp[0];
    $tmp2 = explode(',', $tmp[2]);
    $start = (int)$tmp2[0];
    $start2 = (int)$tmp2[1];
    $tmp2 = explode('x', $tmp[3]);
    $end = $start + $tmp2[0];
    $end2 = $start2 + $tmp2[1];

    for ($i = $start; $i < $end; $i++) {
        for ($j = $start2; $j < $end2; $j++) {
            if (isset($elements[$i][$j]) && $elements[$i][$j] != 'X') {
                $claims[$elements[$i][$j]]++;
                $claims[$name]++;
                $elements[$i][$j] = 'X';
                $count++;
            }
            else if (!isset($elements[$i][$j]))
                $elements[$i][$j] = $name;
            else 
                $claims[$name]++;

        }
    }

}

echo $count . PHP_EOL;

foreach ($names as $name) {
    if (!array_key_exists($name, $claims)) {
        echo $name . PHP_EOL;
        return;
    }
}