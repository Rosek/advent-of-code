<?php

$file = new SplFileObject("in.txt");

while (!$file->eof()) {
    $in[] = $file->fgets();
}

unset($file);

$counter = 0;

foreach ($in as $value) {
    $number = substr($value, 1, strlen($value));

    switch ($value[0]) {
        case '-':
            $counter -= $number;
            break;
        case '+':
            $counter += $number;
            break;
    }
}

echo $counter . PHP_EOL;

$counter = 0;
$frequency = [];

while (1) {
    foreach ($in as $value) {
        $number = substr($value, 1, strlen($value));

        switch ($value[0]) {
            case '-':
                $counter -= $number;
                break;
            case '+':
                $counter += $number;
                break;
            default:
                break 2;
        }

        if (in_array($counter, $frequency)) {
            echo $counter . PHP_EOL;
            return;
        }

        $frequency[] = $counter;
    }
}